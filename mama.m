%Para comentar se emplea el simbolo '%'

%Borro posibles variables guardadas anteriormente y limpio la consola
clc; clear all; close all;

%Defino las constantes que empleare
K=8.6173324e-5; %eV 
KT=K*300*1000; %meV
mu=0; %meV
limE=0.2; %meV, es el limite de energia a partir de la cual considerare que no hay estados electronicos ocupados
hbarra=6.58211899e-13; %meV*s
hbarraJ=1.054571628e-34; %J*s  --  J=kg*m^2*s^-2
hbarraMia=1.054571628e-30; %kg*cm^2*s^-1
m=9.1e-31; %kg
dKx=(2*pi)/0.3; %cm^-1 - Eje X 3 mm
dKy=(2*pi)/0.2; %cm^-1 - Eje Y 2 mm
dKz=(2*pi)/0.1; %cm^-1 - Eje Z 1 mm
cteE=(hbarra*hbarraMia)/(0.2*m);

%Calculo el modulo al cuadrado maximo del vector K que es compatible con el rango de energias empleado, utilizando para ello la aproximacion de bandas parabolicas
KCmax=0.2*m*limE/(hbarra*hbarraMia);
Kmax=sqrt(KCmax);

%Defino vectores con todos los valores posibles de las Kx,Ky,Kz
KX=0:dKx:Kmax;
longKX=length(KX);
KY=0:dKy:Kmax;
longKY=length(KY);
KZ=0:dKz:Kmax;
longKZ=length(KZ);

%Defino la funcion energia y el numero de estados disponibles en el rango de energias que hemos considerado para cada direccion. He puesto como 0 de energia el nivel de Fermi a 300K
E=@(x,y,z)((cteE*((x^2)+(y^2)+(z^2)))); %+100 para situar el 0en el nivel de Fermi
f=@(E)(1/(1+exp((E-mu)/KT)));


%Datos empleados para poder detener el programa y reanudarlo mas adelante por donde iba %%%
Nestados=0;
Nelectrones=0;
cxParada=3000;
limite=5000;
%%%h = waitbar(0,'Initializing waitbar...'); %Crear una barra de progreso

%Y aqui los bucles
for cx=cxParada:limite %(cxParada es para poder reanudar mas adelante el bucle) hasta el numero de valores posibles de Kx que hay redondeado al alza
  kx=KX(cx); 
  for cy=1:longKY %Un segundo bucle que recorra todos los valores de Ky habiendo fijado Kx
    ky=KY(cy);
    for cz=1:longKZ %Un tercer bucle que recorre todos los valores posible de Kz habiendo fijado Kx y Ky
      kz=KZ(cz);
      Energia=E(kx,ky,kz);
      if (Energia<limE)
        if ((kx+ky+kz)==0)
          Nestados+=1;
        elseif(((kx+ky)==0 && kz~=0) || ((kx+kz)==0 && ky~=0) || ((kz+ky)==0 && kx~=0))
          Nestados+=2;
        elseif (((kx~=0 && ky~=0) && kz==0) || ((kz~=0 && ky~=0) && kx==0) || ((kx~=0 && kz~=0) && ky==0))
          Nestados+=4;
        elseif (kx~=0 && ky~=0 && kz~=0)
          Nestados+=8;
        end
      end
      if (Energia>limE)
        break
      end
      %prob=f(Energia);  
      %Nelectrones+=prob; 
    end  
    if (E(kx,ky,0)>limE)
      break
    end
  end
end

%%%close(h) %Cerrar la barra de progreso

%Solo cogimos Kx>0, Ky>0, Kz>0 y no tuvimos en cuenta los espines, por lo que debemos hacer un ultimo calculo
Ne=2*Ne;